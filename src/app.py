import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from pymodm import connect

from src.utils import ResponseModel, ErrorResponse
from src.configs import get_config, check_redis_connection
from src.routers import *

app = FastAPI(openapi_url=None)

# Add middlewares
app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_methods=['GET', 'POST', 'OPTIONS'],
                   allow_headers=['AUTHORIZATION, If-none-match'],
                   expose_headers=['ETag', 'X-TOKEN'],
                   max_age=1728000)

# Include route
app.include_router(misc_router)


@app.exception_handler(RequestValidationError)
def request_model_exception_handler(_, exc: RequestValidationError):
    err = ErrorResponse(
        code=422,
        message=exc.errors()
    )
    return JSONResponse(ResponseModel[ErrorResponse](error=err).dict(), status_code=422)


@app.on_event("startup")
async def startup_event():
    # Check & get configs
    configs = get_config()

    # Init sentry
    # sentry_sdk.init(
    #     dsn=configs.SENTRY_DSN,
    #     environment=configs.SENTRY_ENV
    # )
    # app.add_middleware(SentryAsgiMiddleware)

    # Ping redis server
    check_redis_connection()

    # Connect DB
    connect(mongodb_uri=configs.APP_MONGODB)
