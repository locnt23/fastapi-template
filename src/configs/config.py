from pydantic import BaseSettings


class Settings(BaseSettings):
    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

    APP_REDIS_HOST: str = 'localhost'
    APP_REDIS_PORT: int = 6379
    APP_REDIS_DB: int = 0

    APP_MONGODB: str = ...
    # SENTRY_DSN: str = ...
    # SENTRY_ENV: str = 'Development'


def get_config():
    return Settings()
