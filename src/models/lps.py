import pymongo
from pymodm import MongoModel, fields, manager


class LichPhatSong(MongoModel):

    class Meta:
        collection_name = 'lps'
        final = True
        ignore_unknown_fields = True
        indexes = [
            pymongo.IndexModel([('channel_id', pymongo.ASCENDING)]),
        ]

    _id = fields.ObjectIdField(primary_key=True)
    channel_id = fields.CharField()
    channel_name = fields.CharField()
    start_time = fields.DateTimeField()
    end_time = fields.DateTimeField()
